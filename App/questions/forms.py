
from secrets import choice
from flask_wtf import FlaskForm

from wtforms import StringField, SubmitField, TextAreaField,SelectField
from wtforms.validators import DataRequired
from App.models import subject







class AddQuestionForm(FlaskForm):
    
    subject = SelectField('Subject', validators=[DataRequired()],choices=[])
    question = TextAreaField('Question', validators=[DataRequired()])
    option1 = StringField('Option 1', validators=[DataRequired()])
    option2 = StringField('Option 2', validators=[DataRequired()])
    option3 = StringField('Option 3', validators=[DataRequired()])
    option4 = StringField('Option 4', validators=[DataRequired()])
    answer = StringField('Answer', validators=[DataRequired()])
    submit = SubmitField('Add Question')

    def __init__(self, *args, **kwargs):
        super(AddQuestionForm, self).__init__(*args, **kwargs)
        self.subject.choices = [(subject.id, subject.name) for subject in subject.query.all()]

# update Question form

class UpdateQuestionForm(FlaskForm):
    subject = StringField('Subject', validators=[DataRequired()])
    question = TextAreaField('Question', validators=[DataRequired()])
    option1 = StringField('Option 1', validators=[DataRequired()])
    option2 = StringField('Option 2', validators=[DataRequired()])
    option3 = StringField('Option 3', validators=[DataRequired()])
    option4 = StringField('Option 4', validators=[DataRequired()])
    answer = StringField('Answer', validators=[DataRequired()])
    submit = SubmitField('Update Question')
