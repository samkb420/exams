
from flask import (render_template, url_for, flash,
                   redirect, request, abort, Blueprint)
from flask_login import current_user, login_required
from App import db

from App.models import Questions
from App.questions.forms import AddQuestionForm, UpdateQuestionForm


questions = Blueprint('questions', __name__)


@questions.route("/add_question", methods=['GET', 'POST'])
# @login_required
def add_question():
    form = AddQuestionForm()
    if form.validate_on_submit():
        question = Questions(subject=form.subject.data,
                             question=form.question.data,
                             option1=form.option1.data,
                             option2=form.option2.data,
                             option3=form.option3.data,
                             option4=form.option4.data,
                             answer=form.answer.data)
        db.session.add(question)
        db.session.commit()
        flash('Question added successfully', 'success')
        return redirect(url_for('main.home'))
    return render_template('Admin/add_question.html', title='Add Question', form=form, legend='Add Question')


@questions.route("/question/<int:qid>")
def question(qid):
    question = Questions.query.get_or_404(qid)
    return render_template('question.html', title=question.question, question=question)

# update question
@questions.route("/question/<int:qid>/update", methods=['GET', 'POST'])
@login_required
def update_question(qid):
 
    form = UpdateQuestionForm()
    if form.validate_on_submit():
        question.subject = form.subject.data
        question.question = form.question.data
        question.option1 = form.option1.data
        question.option2 = form.option2.data
        question.option3 = form.option3.data
        question.option4 = form.option4.data
        question.answer = form.answer.data
        db.session.commit()
        flash('Question updated successfully', 'success')
        return redirect(url_for('questions.question', qid=question.id))
    elif request.method == 'GET':
        form.subject.data = question.subject
        form.question.data = question.question
        form.option1.data = question.option1
        form.option2.data = question.option2
        form.option3.data = question.option3
        form.option4.data = question.option4
        form.answer.data = question.answer
    return render_template('add_question.html', title='Update Question', form=form, legend='Update Question')

# all questions
@questions.route("/questions")
# @login_required
@login_required
def all_questions():
    page = request.args.get('page', 1, type=int)
    questions = Questions.query.order_by(Questions.qid.asc()).paginate(page=page, per_page=1)
    return render_template('exam.html', questions=questions)

# # delete question
# @questions.route("/question/<int:qid>/delete", methods=['POST'])
# 
# def delete_question(qid):
#     question = Questions.query.get_or_404(qid)
#     db.session.delete(question)
#     db.session.commit()
#     flash('Question deleted successfully', 'success')
#     return redirect(url_for('questions.questions'))





         








